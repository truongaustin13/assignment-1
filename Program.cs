﻿// See https://aka.ms/new-console-template for more information
class NguoiLaoDong{
    public string HoTen{
        get; set;
    }
    public System.DateTime NamSinh{
        get; set;
    }
    public double LuongCoBan{
        get; set;
    }
    public NguoiLaoDong(){
        this.HoTen = "";
        this.NamSinh = default;
        this.LuongCoBan = 0;
    }
    public NguoiLaoDong(string HoTen, System.DateTime NamSinh, double LuongCoBan){
        this.HoTen = HoTen;
        this.NamSinh = NamSinh;
        this.LuongCoBan = LuongCoBan;
    }
    public double TinhLuong(){
        return this.LuongCoBan;
    }
    public void XuatThongTin(){
        Console.WriteLine($"Ho ten la: {this.HoTen}, nam sinh: {this.NamSinh}, luong co ban: {this.LuongCoBan}.");
    }
}

class GiaoVien : NguoiLaoDong{
    public double HeSoLuong{
        get; set;
    }
    public GiaoVien(){
    }
    public GiaoVien(string HoTen, System.DateTime NamSinh, double LuongCoBan, double HeSoLuong){
        this.HoTen = HoTen;
        this.NamSinh = NamSinh;
        this.LuongCoBan = LuongCoBan;
        this.HeSoLuong = HeSoLuong;
    }
    public void NhapThongTin(double HeSoLuong){
        this.HeSoLuong = HeSoLuong;
    }
    public new double TinhLuong(){
        return this.LuongCoBan * this.HeSoLuong * 1.25;
    }

    public new void XuatThongTin(){
        double Luong = TinhLuong();
        Console.WriteLine($"Ho ten la: {this.HoTen}, nam sinh: {this.NamSinh}, luong co ban: {this.LuongCoBan}, he so luong: {this.HeSoLuong}, luong: {Luong}.");
    }
    public void XuLy(){
        this.HeSoLuong = this.HeSoLuong + 6;
    }
    
}

class Program{
    static string SafeStringInput(){
        string? input = "";
        while (true){
            try {
                input = Console.ReadLine();
                if (!string.IsNullOrEmpty(input)){
                    break;
                }
                else throw new System.Exception("Input string is empty. Please re-enter a valid string.");
            }
            catch (System.Exception){
                Console.WriteLine("Input string is empty. Please re-enter a valid string.");
                //Console.Clear();
                continue;
            }
        }
        return input;
    }
    static int SafeIntInput(){
        int input = 0;
        bool success = false;
        while(true){
            try
            {
                success = int.TryParse(SafeStringInput(), out input);
                if (success is true) 
                    break;
                else throw new System.FormatException("Invalid format");
            }
            catch (System.Exception)
            {
                Console.WriteLine("Input format is not an integer. Please enter a valid number.");
                continue;
            }
        }
        return input;
    }
    static double SafeDoubleInput(){
        double result = 0;
        bool success = false;
        while (true){
            try {
                success = double.TryParse(SafeStringInput(), out result);
                if (success is true) {
                    break;
                }
                else throw new System.FormatException("Invalid format.");
            }
            catch (System.FormatException){
                Console.WriteLine("Input format is not a double. Please re-enter a valid number.");
                //Console.Clear();
                continue;
            }
        }
        return result;
    }
    static System.DateTime SafeDateTimeInput(){
        System.DateTime result;
        bool success = false;
        while (true){
            try {
                success = System.DateTime.TryParseExact(Console.ReadLine(),"dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,System.Globalization.DateTimeStyles.None , out result);
                if (success is true) {
                    break;
                }
                else throw new System.Exception("Input date of birth is not valid. Please re-enter a valid date of birth.");
            }
            catch (System.Exception){
                Console.WriteLine("Input date of birth is not valid. Please re-enter a valid date of birth.");
                //Console.Clear();
                continue;
            }
        }
        return result;
    }


    static void Main(string[] args){
        Console.Clear();
        Console.WriteLine("------------------------------------------------------------");
        Console.WriteLine("Nhap so luong giao vien can thiet: ");
        int ListSize = SafeIntInput();
        GiaoVien[] ListGiaoVien = new GiaoVien[ListSize];
        Console.WriteLine("------------------------------------------------------------");


        for (int i = 0; i < ListSize; i++){
            Console.WriteLine("Ten nhan vien: "); 
            string input_name = SafeStringInput();
            
            Console.WriteLine("Ngay thang nam sinh theo dinh dang ngay/thang/nam: "); 
            System.DateTime input_dob = SafeDateTimeInput();
            
            Console.WriteLine("Luong co ban: ");
            double input_salary = SafeDoubleInput();

            Console.WriteLine("He so luong: "); 
            double input_salary_coefficient = SafeDoubleInput();

            ListGiaoVien[i] = new GiaoVien(input_name, input_dob, input_salary, input_salary_coefficient);
            Console.WriteLine("------------------------------------------------------------");
        }
        Console.WriteLine("Nhap thanh cong!");
        Console.WriteLine("------------------------------------------------------------");
        Console.WriteLine("Giao vien co luong thap nhat:");
        double lowest_base_salary = 1e5;
        //int[] index_list = new int [ListSize]; 
        for (int i =0 ; i <ListSize; i++){
            if (ListGiaoVien[i].LuongCoBan < lowest_base_salary) {
                lowest_base_salary = ListGiaoVien[i].LuongCoBan;

            }
        }
        for (int i = 0 ; i <ListSize; i++){
            if (ListGiaoVien[i].LuongCoBan == lowest_base_salary)
                Console.WriteLine(ListGiaoVien[i].HoTen + " " + ListGiaoVien[i].NamSinh.ToString("dd/MM/yyyy") + " " + ListGiaoVien[i].LuongCoBan + " " + ListGiaoVien[i].HeSoLuong);
        }
        Console.ReadKey();
        //Console.Clear();
    }
}
